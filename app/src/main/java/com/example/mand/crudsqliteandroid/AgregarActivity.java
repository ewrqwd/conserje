package com.example.mand.crudsqliteandroid;


import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import BSHELPER.sqlite;
public class AgregarActivity extends AppCompatActivity{
    private EditText cedula, nombre,apellidos,fechanacimiento, facultadlabor;
    public void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.activity_agregar);

        cedula = (EditText) findViewById(R.id.ETCedula);
        nombre = (EditText) findViewById(R.id.ETNombre);
        apellidos = (EditText) findViewById(R.id.ETApellidos);
        fechanacimiento = (EditText) findViewById(R.id.ETFechaNacimiento);
        facultadlabor = (EditText) findViewById(R.id.ETFacultadLabor);
    }
    public void agregar(View v){
        if(ComprobarCampos()){
            String ced, nom,ape, facul;
            int fechanaci;
            ced = cedula.getText().toString();
            nom = nombre.getText().toString();
            ape = apellidos.getText().toString();
            fechanaci = Integer.parseInt(fechanacimiento.getText().toString());
            facul =facultadlabor.getText().toString();

            sqlite bh = new sqlite(AgregarActivity.this,"usuarios",null,1);
            if(bh!=null){
                SQLiteDatabase db = bh.getWritableDatabase();
                ContentValues con = new ContentValues();
                con.put("cedula",ced);
                con.put("nombre",nom);
                con.put("apellidos",ape);
                con.put("fechanacimiento",fechanaci);
                con.put("facultadlabor",facul);
                long insertado = db.insert("usuarios",null,con);
                if(insertado>0){
                    Toast.makeText(AgregarActivity.this,"Insertado con exito",Toast.LENGTH_SHORT).show();
                    cedula.setText("");
                    nombre.setText("");
                    apellidos.setText("");
                    fechanacimiento.setText("");
                    facultadlabor.setText("");
                }else{
                    Toast.makeText(AgregarActivity.this,"No se inserto",Toast.LENGTH_SHORT).show();
                }
            }
        }else{
            Toast.makeText(AgregarActivity.this,"hay campos vacios",Toast.LENGTH_LONG).show();
        }
    }
    public boolean ComprobarCampos(){
        if(cedula.getText().toString().isEmpty() || nombre.getText().toString().isEmpty() || apellidos.getText().toString().isEmpty() ||facultadlabor.getText().toString().isEmpty() || fechanacimiento.getText().toString().isEmpty()){
            return false;
        }else{
            return true;
        }
    }
}
